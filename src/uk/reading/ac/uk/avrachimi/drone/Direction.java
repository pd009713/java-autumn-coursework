package uk.reading.ac.uk.avrachimi.drone;

import java.util.Random;
/**
 * Enum class that return either random direction or next direction
 * @author pd009713
 *
 */

public enum Direction {
		NORTH, EAST, SOUTH, WEST;
	
	/**
	 * 
	 * @return random direction
	 */
	public static Direction getRandom() {
		Random random = new Random();
		return values()[random.nextInt(values().length)]; // Return random direction
	}
	
	/**
	 * Returns next direction
	 * <p>
	 * Examples:
	 * If dir is NORTH then next dir is EAST
	 * If dir is WEST then next dir is NORTH
	 * </p>
	 * @return next direction
	 */
	public Direction next() {
		if (this.ordinal() == 3)
			return values()[0];
		else
			return values()[this.ordinal() + 1];
	}
	
	/**
	 * Used to test the class
	 * @param args
	 */
	public static void main(String[] args) {
		Direction dir = Direction.getRandom();
		System.out.println(dir.toString());
		System.out.println(dir.next());
	}
}
