package uk.reading.ac.uk.avrachimi.drone;

import java.util.ArrayList;
import java.util.Random;
/**
 * Arena class that holds all the drones
 * @author pd009713
 *
 */

public class DroneArena {

	private int sizeX, sizeY;

	Random random;
	ArrayList<Drone> drones = new ArrayList<Drone>();
	
	/**
	 * DroneArena constructor
	 * @param x		X size
	 * @param y		Y size
	 */
	public DroneArena(int x, int y) {
		// Create new drone to access initializeInstance method
		// initialiazeInstance sets static variable droneCounter = 1
		Drone d = new Drone(1, 1, Direction.WEST);
		d.initializeInstance();
		
		this.sizeX = x;
		this.sizeY = y;
		random = new Random();
	}

	/**
	 * Adds drone with random position in the drones list
	 * <p>
	 * New drone is added on empty position. If there is no empty position, then drone is not added and user is informed.
	 * </p>
	 */
	public void addDrone() {
		int randomX;
		int randomY;
		int maxPositions = sizeX * sizeY;

		if (drones.size() < maxPositions) { // Run only if there is empty space
											// for a new drone to be put
			// Loop until the random X,Y position is empty
			do {
				randomX = random.nextInt(this.sizeX);
				randomY = random.nextInt(this.sizeY);
			} while (getDroneAt(randomX, randomY) != null);

			// Add drone to list
			Direction direction = Direction.getRandom();
			Drone d = new Drone(randomX, randomY, direction);
			drones.add(d);
			System.out.println("\nDrone " + d.getID() + " added at (" + randomX + "," + randomY + ") with direction " + direction + "\n");
		} else {
			System.out.println("\nThe arena is already full with drones. You can't add another drone.\n");
		}

	}

	/**
	 * Returns size of Arena and each Drone's details
	 * @return Arena and Drone details
	 */
	public String toString() {
		String output = "This arena is " + this.sizeX + "x" + this.sizeY + "\n";

		for (int i = 0; i < drones.size(); i++) {
			output += "Drone " + drones.get(i).toString() + "\n";
		}

		return output;
	}

	/**
	 * search arraylist of drones to see if there is a drone at x,y
	 * 
	 * @param x		Drone pos X
	 * @param y		Drone pos Y
	 * @return null if no Drone there, otherwise return drone
	 */
	public Drone getDroneAt(int x, int y) {

		for (int i = 0; i < drones.size(); i++) {
			if (drones.get(i).isHere(x, y))
				return drones.get(i);
		}
		return null;
	}

	/**
	 * Displays all drones
	 * @param c ConsoleCanvas object
	 * @see Drone#displayDrone(ConsoleCanvas)
	 */
	public void showDrones(ConsoleCanvas c) {

		for (int i = 0; i < drones.size(); i++) {
			drones.get(i).displayDrone(c);
		}
	}

	/**
	 * 
	 * @return X size of arena
	 */
	public int getX() {
		return sizeX;
	}

	/**
	 * 
	 * @return Y size of arena
	 */
	public int getY() {
		return sizeY;
	}


	/**
	 * Checks if drone can move to the given position
	 * @param x
	 * @param y
	 * @return false if drone can't move to that position, otherwise true
	 */
	public boolean canMoveHere(int x, int y) {

		if (getDroneAt(x, y) != null || x >= sizeX || y >= sizeY || x < 0 || y < 0)
			return false;

		return true;
	}

	/**
	 * Moves all drones once in their given direction
	 * @see Drone#tryToMove(DroneArena)
	 */
	public void moveAllDrones() {

		for (Drone d : drones) {
			d.tryToMove(this);
		}
	}
	
	/**
	 * Used to test class
	 * @param args
	 */
	public static void main(String[] args) {

		DroneArena a = new DroneArena(20, 10); // create drone arena

		for (int i = 0; i < 3; i++)
			a.addDrone();

		System.out.println(a.toString()); // print where is
	}

}