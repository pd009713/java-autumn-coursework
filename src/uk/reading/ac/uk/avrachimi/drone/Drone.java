package uk.reading.ac.uk.avrachimi.drone;
/**
 * Drone Class
 * <p>
 * X is horizontal (or column)
 * Y is vertical (or row)
 * </p>
 * @author pd009713
 *
 */

public class Drone {
	
	private int posX, posY;
	private Direction dir;
	private int ID;
	private static int droneCounter = 1;
	
	/**
	 * 
	 * @param x		x position
	 * @param y		y position
	 * @param direction 
	 * @see Direction
	 */
	public Drone (int x, int y, Direction direction) {
		// Initialize variables
		this.posX = x;
		this.posY = y;
		this.dir = direction;
		this.ID = droneCounter++;
	}
	
	/**
	 * @return Drone position and direction as a string
	 */
	public String toString() {
		return "Drone " + this.ID + " is at (" + this.posX + "," + this.posY + ") with direction " + dir.toString();
	}
	
	/**
	 * Is the drone at this x,y position
	 * @param sx	x position
	 * @param sy	y position
	 * @return		true if drone is at sx,sy, false otherwise
	 */
	public boolean isHere (int sx, int sy) {
		if (sx == this.posX && sy == this.posY)
			return true;
		else
			return false;
	}
	
	/**
	 * 
	 * @param c		ConsoleCanvas object reference
	 * @see ConsoleCanvas#showIt(int, int, String)
	 */
	public void displayDrone(ConsoleCanvas c) {
		c.showIt(posX, posY, "D");
	}
	
	/**
	 * Moves drone on position in the given direction.
	 * <p>
	 * Checks if drone can move in the given direction. If not, changes direction of drone.
	 * Reasons for not being able to move: 1. End of Arena  2. Another drone is already at that position
	 * </p>
	 * @param a		DroneArena object
	 * 
	 * @see DroneArena#canMoveHere(int, int)
	 * @see Direction#next()
	 */
	public void tryToMove(DroneArena a) {
		
		switch(dir) {
			case NORTH:
				if (a.canMoveHere(posX, posY - 1))
					posY -= 1; // Change position
				else
					dir = dir.next(); // Next direction
				break;
			case EAST:
				if (a.canMoveHere(posX + 1, posY))
					posX += 1;
				else
					dir = dir.next();
				break;
			case SOUTH:
				if (a.canMoveHere(posX, posY + 1))
					posY += 1;
				else
					dir = dir.next();
				break;
			case WEST:
				if (a.canMoveHere(posX - 1, posY))
					posX -= 1;
				else
					dir = dir.next();
				break;
			default:
				break;
		}
	}
	
	/**
	 * 
	 * @return X position of Drone
	 */
	public int getX() {
		return posX;
	}
	
	/**
	 * 
	 * @return Y position of Drone
	 */
	public int getY() {
		return posY;
	}
	
	/**
	 * 
	 * @return direction of Drone
	 */
	public Direction getDir() {
		return this.dir;
	}
	
	/**
	 * 
	 * @return Drone ID
	 */
	public int getID() {
		return ID;
	}
	
	/**
	 * Initializes the droneCounter variable
	 * <p>
	 * This is used when a new arena is created or when an arena is loaded from a file.
	 * </p>
	 * @see DroneArena#DroneArena(int, int)
	 */
	public void initializeInstance() {
		droneCounter = 1;
	}
	
	/**
	 * Used to test class
	 * @param args
	 */
	public static void main(String[] args) {
		
		Drone d = new Drone(5, 3, Direction.WEST);		// create drone
		Drone d1 = new Drone(1, 1, Direction.NORTH);
		System.out.println(d.toString());
		System.out.println(d1.toString());
		d.dir = d.dir.next();
		System.out.println(d.toString());
	}

}