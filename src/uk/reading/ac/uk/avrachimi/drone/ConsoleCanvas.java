package uk.reading.ac.uk.avrachimi.drone;
/**
 * 
 * @author pd009713
 *
 */

public class ConsoleCanvas {
	
	String[][] canvas;
	
	/**
	 * ConsoleCanvas constructor
	 * @param column	X size
	 * @param row	 	Y size
	 */
	public ConsoleCanvas(int column, int row) {
		
		this.canvas = new String[row][column]; // Create new String array as a canvas
		
		// Add hashtags as the canvas border and spaces in the middle
		// ########
		// #      #
		// #      #
		// ########
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				if (i == 0 || i == row - 1) {
					this.canvas[i][j] = "#";
				}
				else if (j == 0|| j == column - 1) {
					this.canvas[i][j] = "#";
				}
				else {
					this.canvas[i][j] = " ";
				}
			}
		}
	}
	
	/**
	 * Puts given character on given position in the canvas array
	 * @param x				X position of drone
	 * @param y				Y position of drone
	 * @param displayChar	Character to be displayed on console
	 */
	public void showIt(int x, int y, String displayChar) {
		this.canvas[y + 1][x + 1] = displayChar; // No need to check if x,y is in limits because of the AddDrone function in DroneArena
	}
	
	/**
	 * @return Canvas with drones and borders
	 */
	public String toString() {
		
		String output = "";
		
		for (int row = 0; row < this.canvas.length; row++) {
			for (int column = 0; column < this.canvas[0].length; column++) {
				output += this.canvas[row][column];
			}
			output += "\n";
		}
		
		return output;
	}
	
	/**
	 * Used for testing class
	 * @param args
	 */
	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas (40, 10);	// create a canvas
		c.showIt(8,3,"D");								// add a Drone at 4,3
		System.out.println(c.toString());				// display result
	}

}