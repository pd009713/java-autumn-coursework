package samplepackage;

import java.util.ArrayList;
import java.util.Random;

public class DroneArena {
	
	private int sizeX, sizeY;
	
	Random random;
	ArrayList<Drone> drones = new ArrayList<Drone>();
	
	public DroneArena(int x, int y) {
		this.sizeX = x;
		this.sizeY = y;
		random = new Random();
	}
	
	// Adds drone with random position to the drones list
	public void addDrone() {
		int randomX;
		int randomY;
		int maxPositions = sizeX * sizeY;
		
		if (drones.size() < maxPositions) { // Run only if there is empty space for a new drone to be put
			// Loop until the random X,Y position is empty
			do {
				randomX = random.nextInt(this.sizeX);
				randomY = random.nextInt(this.sizeY);
			} while (getDroneAt(randomX, randomY) != null);
			
			// Add drone to list
			Direction direction = Direction.getRandom();
			Drone d = new Drone(randomX, randomY, direction);
			drones.add(d);
			System.out.println("\nDrone added at (" + randomX + "," + randomY + ") with direction " + direction + "\n");
		}
		else {
			System.out.println("\nThe arena is already full with drones. You can't add another drone.\n");
		}
		
	}
	
	// Print details of this arena and of each drone
	public String toString() {
		String output = "This arena is " + this.sizeX + "x" + this.sizeY + "\n";
		
		for (int i = 0; i < drones.size(); i++) {
			output += "Drone " + drones.get(i).toString() + "\n";
		}
		
		return output;
	}
	
	/**
	 * search arraylist of drones to see if there is a drone at x,y
	 * @param x
	 * @param y
	 * @return null if no Drone there, otherwise return drone
	 */
	// If drone is at given position, return it. Otherwise return null
	public Drone getDroneAt(int x, int y) {
		
		for (int i = 0; i < drones.size(); i++) {
			if (drones.get(i).isHere(x, y))
				return drones.get(i);
		}
		return null;
	}
	
	// Loop through drones and display each one
	public void showDrones(ConsoleCanvas c) {
		
		for (int i = 0; i < drones.size(); i ++) {
			drones.get(i).displayDrone(c);
		}
	}
	
	public void clearArena() {
		drones.clear();
		drones =  new ArrayList<Drone>();
	}
	
	// Get X and Y size of arena
	public int getX() {
		return sizeX;
	}
	
	public int getY() {
		return sizeY;
	}
	
	// Check if drone can move to new position
	public boolean canMoveHere(int x, int y) {
		
		if (getDroneAt(x,y) != null || x >= sizeX || y >= sizeY || x < 0 || y < 0)
			return false;
		
		return true;
	}
	
	// Move all drones in their specified direction
	public void moveAllDrones() {
		
		for (Drone d : drones) {
			d.tryToMove(this);
		}
	}
	
	public static void main(String[] args) {
		
		DroneArena a = new DroneArena(20, 10);	// create drone arena
		
		for (int i = 0; i < 3; i++) 
			a.addDrone(); 
		
		System.out.println(a.toString());	// print where is
	}

}

