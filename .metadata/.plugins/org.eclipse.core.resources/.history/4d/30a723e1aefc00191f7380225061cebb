package samplepackage;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.io.*;

//import javax.swing.*; 
import javax.swing.JFileChooser; 
import javax.swing.filechooser.*;
import javax.swing.filechooser.FileFilter;

public class DroneInterface {
	
	private Scanner s;								// scanner used for input from user
 	private DroneArena myArena;				// arena in which drones are shown
 	
    /**
    	 * constructor for DroneInterface
    	 * sets up scanner used for input and the arena
    	 * then has main loop allowing user to enter commands
     */
    public DroneInterface() {
    	myArena = new DroneArena(10, 10);	// create arena of size 20*6
    	System.out.println("----------------------------------------\n");
    	System.out.println("Program started with empty Arena of size 10x10.\n");
    	
    	mainMenu();
    	
    }
    
    // Displays main menu
    void mainMenu() {
    	s = new Scanner(System.in);			// set up scanner for user input
    	char ch = ' ';
        
        while (ch != '8') {
	        	System.out.println("----------------------------------------");
	        	System.out.println("----------------------------------------");
	        	System.out.println("1. Add Drone");
	        	System.out.println("2. Drone Information");
	        	System.out.println("3. Display Drones");
	        	System.out.println("4. Move Drones Once");
	        	System.out.println("5. Move Drones 10 Times");
	        	System.out.println("6. Create new Arena");
	        	System.out.println("7. Open/Save File");
	        	System.out.println("8. Exit Program");
	        	System.out.print("Enter the value of your selected option: ");
	        	ch = s.next().charAt(0);
	            s.nextLine();
	        
	       	switch (ch) {
	   			case '1' :
	   				myArena.addDrone();	// Add a new drone to arena
	      			break;
	       		case '2' :
	        		System.out.print("\n" + myArena.toString() + "\n"); // Print arena and drone details
	           		break;
	       		case '3':
	       			if (myArena.drones.size() > 0) {
	       				doDisplay(); // Display arena with drones
	       			}
	       			else {
	       				System.err.println("\nThere are no drones in the arena to move.\n");
	       			}
	       			break;
	       		case '4':
	       			if (myArena.drones.size() > 0) {
	       				myArena.moveAllDrones(); // Move all drones
	       				doDisplay(); // Display new arena
	       			}
	       			else {
	       				System.err.println("\nThere are no drones in the arena to move.\n");
	       			}
	        		break;
	        	case '5':
	       			// Move drones, display arena 10 times
	        		if (myArena.drones.size() > 0) {
		       			for (int i = 0; i < 10; i++) {
		       				System.out.println("-----------------------------------");
		       				myArena.moveAllDrones();
		           			doDisplay();
		           			System.out.print(myArena.toString() + "\n");
		           			try {
		           	            TimeUnit.MILLISECONDS.sleep(200); // Wait for 200ms
		           	        } 
		           			catch (InterruptedException e) {
		           	            System.err.format("IOException: %s%n", e);
		                    }
		        		}
	        		}
	        		else {
	        			System.err.println("\nThere are no drones in the arena to move.\n");
	        		}
	        		break;
	        	case '6':
	        		int x,y;
	        		myArena.clearArena(); // Clear all drones from the arena
	        		String input = "";
	        		System.out.print("Enter new Arena's X value: ");
	        		input = s.next();
	        		x = Integer.parseInt(input);
		            s.nextLine();
		            System.out.print("Enter new Arena's X value: ");
	        		input = s.next();
	        		y = Integer.parseInt(input);;
		            s.nextLine();
		            ch = '6';
		            myArena = new DroneArena(x,y);
	        		System.out.println("\nArena successfully created with X = " + x + " and Y = " + y);
	        		System.out.println();
	        		break;
	        	case '7':
	        		fileMenu(); // Show file menu (save, open)
	        		break;
	        	case '8' :
	        		System.err.println("Program Terminated");
	        		break;
	        	default:
	        		break;
	        }
        }
        
       s.close(); // Close scanner
    }
    
    void fileMenu() {
    	s = new Scanner(System.in); // Set up scanner for user input
    	char ch = ' ';
        
        while (ch != '3') {
        	System.out.println("----------------------------------------");
        	System.out.println("----------------------------------------");
        	System.out.println("1. Open");
        	System.out.println("2. Save");
        	System.out.println("3. Back");
        	System.out.println("Enter the value of your selected option: ");
        	ch = s.next().charAt(0);
            s.nextLine();
        
	        switch(ch) {
	        	case '1':
	        		try {
	        			openFile();
	        			ch = '3';
	        		}
	        		catch (Exception e){
	        			System.out.println();
	        			System.err.println("Something went wrong. Make sure you have selected a valid file/path.");
	        			System.out.println();
	        		}
	        		break;
	        	case '2':
	        		try {
	        			saveFile();
	        			ch = '3';
	        		}
	        		catch (Exception e){
	        			System.out.println();
	        			System.err.println("Something went wrong. Make sure you have selected a valid file/path.");
	        			System.out.println();
	        		}
	        		break;
	        	case '3':
	        		break;
	        	default:
	        		break;
	        }
        }
    }
    
    // Save file to the directory chosen by the user
    void saveFile() throws Exception{
    	
    	// Open File Chooser window for user to choose directory/file
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		jfc.setDialogTitle("Choose a directory to save the arena: ");
		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnValue = jfc.showSaveDialog(null);
		    	
		s = new Scanner(System.in);
		String input = "";
		System.out.print("Please enter the name of the file: ");
		input = s.next();
		
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			if (jfc.getSelectedFile().isDirectory()) {
				try {
					File file = new File(jfc.getCurrentDirectory() + "/" + input + ".arena"); // User chooses name for file
					
					BufferedWriter writer = new BufferedWriter(new FileWriter(file));
					
					// Write Arena dimensions to file
					writer.write(Integer.toString(myArena.getX()));
					writer.write(" ");
					writer.write(Integer.toString(myArena.getY()));
					writer.newLine();
					
					for (Drone d : myArena.drones) {
						// Drone: posX, posY, dir.ordinal()
						// File:    5    2         0       <-- X{space}Y{space}dir.ordinal
						// New line
						writer.write(Integer.toString(d.getX()));
						writer.write(" ");
						writer.write(Integer.toString(d.getY()));
						writer.write(" ");
						writer.write(Integer.toString(d.getDir().ordinal()));
						writer.newLine();
					}
					writer.close();
					System.out.println();
        			System.out.println("File successfully saved.");
        			System.out.println();
				}
				catch (Exception e) {
					System.out.println();
					System.err.println("Error: Didn't save file");
					System.out.println();
				}		
			}
		}
		else {
			System.out.println();
			System.err.println("File not saved");
			System.out.println();
		}
    }
    
    void openFile() throws IOException{
    	
    	// Open file chooser window
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		// Set file filter to show only directories and .arena files
		jfc.setFileFilter(new FileFilter() {
			public String getDescription() {
				return "Arena files (*.arena)";
			}
			
			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
			    }
				else {
					String filename = f.getName().toLowerCase();
			        return filename.endsWith(".arena");
			   }
			}
		});
		
		int returnValue = jfc.showOpenDialog(null);
		
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			if (jfc.getSelectedFile().isFile()) {
				try {
					BufferedReader reader = new BufferedReader(new FileReader(jfc.getSelectedFile()));
		
					String st;
					st = reader.readLine();
					// Create new arena with saved dimensions
					int arenaX,arenaY;
					String[] arenaSize = st.split(" ");
					arenaX = Integer.parseInt(arenaSize[0]);
					arenaY = Integer.parseInt(arenaSize[1]);
					myArena.clearArena();
					myArena = new DroneArena(arenaX, arenaY);
					st = reader.readLine();
					while (st != null) {
						String[] numbers = st.split(" ");
						int x = Integer.parseInt(numbers[0]);
						int y = Integer.parseInt(numbers[1]);
						int ordinal = Integer.parseInt(numbers[2]);
						myArena.drones.add(new Drone(x, y, Direction.values()[ordinal])); // Add drone to the arena
								
						st = reader.readLine();
					}
							
					reader.close();
					System.out.println();
        			System.out.println("File successfully loaded.");
        			System.out.println();
				}
				catch (Exception e) {
					System.out.println();
					System.err.println("Error: Didn't load file");
					System.out.println();
				}
			}
		}
		else {
			System.out.println();
			System.err.println("File not loaded");
			System.out.println();
		}
	}
    
    void doDisplay() {
		// Determine the arena size 
    	// Hence create a suitable sized ConsoleCanvas object
    	// Call showDrones suitably
    	// Then use the ConsoleCanvas.toString method 
    	
    	ConsoleCanvas c = new ConsoleCanvas(myArena.getX() + 2, myArena.getY() + 2);
    	myArena.showDrones(c);
    	System.out.println("\n" + c.toString() + "\n");
    }

    
	public static void main(String[] args) {
		DroneInterface r = new DroneInterface();	// Just call the interface
	}

}