package samplepackage;
/**
 * Drone Class
 * 
 * @author pd009713
 *
 */

public class Drone {
	
	private int posX, posY;
	private Direction dir;
	private int ID;
	private static int droneCounter = 1;
	
	/**
	 * 
	 * @param x		x position
	 * @param y		y position
	 * @param direction 
	 * @see Direction Class
	 */
	public Drone (int x, int y, Direction direction) {
		// Initialize variables
		this.posX = x;
		this.posY = y;
		this.dir = direction;
		this.ID = droneCounter++;
	}
	
	/**
	 * @return Drone position and direction as a string
	 */
	public String toString() {
		return "Drone " + this.ID + " is at (" + this.posX + "," + this.posY + ") with direction " + dir.toString();
	}
	
	/**
	 * Is the drone at this x,y position
	 * @param sx	x position
	 * @param sy	y position
	 * @return		true if drone is at sx,sy, false otherwise
	 */
	public boolean isHere (int sx, int sy) {
		if (sx == this.posX && sy == this.posY)
			return true;
		else
			return false;
	}
	
	/**
	 * 
	 * @param c		ConsoleCanvas object reference
	 * @see ConsoleCanvas Class
	 */
	public void displayDrone(ConsoleCanvas c) {
		c.showIt(posX, posY, "D");
	}
	
	/**
	 * Moves drone on position in the given direction.
	 * <p>
	 * Checks if drone can move in the given direction. If not, changes direction of drone.
	 * Reasons for not being able to move: 1. End of Arena  2. Another drone is already at that position
	 * </p>
	 * @param a		DroneArena object
	 * 
	 * @see DroneArena Class
	 * @see Direction Class
	 */
	public void tryToMove(DroneArena a) {
		
		switch(dir) {
			case NORTH:
				if (a.canMoveHere(posX, posY - 1))
					posY -= 1; // Change position
				else
					dir = dir.next(); // Next direction
				break;
			case EAST:
				if (a.canMoveHere(posX + 1, posY))
					posX += 1;
				else
					dir = dir.next();
				break;
			case SOUTH:
				if (a.canMoveHere(posX, posY + 1))
					posY += 1;
				else
					dir = dir.next();
				break;
			case WEST:
				if (a.canMoveHere(posX - 1, posY))
					posX -= 1;
				else
					dir = dir.next();
				break;
			default:
				break;
		}
	}
	
	// Return X and Y positions
	public int getX() {
		return posX;
	}
	
	public int getY() {
		return posY;
	}
	
	public Direction getDir() {
		return this.dir;
	}
	
	public int getID() {
		return ID;
	}
	
	public void initializeInstance() {
		droneCounter = 1;
	}
	
	public static void main(String[] args) {
		
		Drone d = new Drone(5, 3, Direction.WEST);		// create drone
		Drone d1 = new Drone(1, 1, Direction.NORTH);
		System.out.println(d.toString());
		System.out.println(d1.toString());
		d.dir = d.dir.next();
		System.out.println(d.toString());
	}

}
